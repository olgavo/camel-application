package com.camel.example;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class SimpleRouter extends RouteBuilder {

    @Override
    public void configure() {
        from("file:src/main/resources/input/")
                .split().xpath("//order[@product='Oil']")
                .process(new FileProcessor())
                .to("file:src/main/resources/output/");
    }
}
