package com.camel.example;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

@Component
public class FileProcessor implements Processor  {

    @Override
    public void process(Exchange exchange) {
        String payload = exchange.getIn().getBody(String.class);
        payload = payload.toUpperCase();
        exchange.getIn().setBody(payload);
    }
}
